from tkinter import *
from tkinter import filedialog

root=Tk("Notepad")
text=Text(root)

root.title("Notepad")

root.config()

myFile = None

def openFile():
    global text
    global myFile
    text.delete('1.0', END)
    t = text.get("1.0", "end-1c")
    myFile = filedialog.askopenfile(mode='r+')
    text.delete('1.0', END)
    text.insert(INSERT, myFile.read())
button = Button(root, text="Open", command=openFile)
button.grid(row = 0, column = 0, sticky = W)
button.config(bd = 0, bg = "#e5e6e8")

def save():
    global text
    global myFile
    t = text.get("1.0", "end-1c")
    myFile.seek(0)
    myFile.write(t)
    myFile.truncate()
    

button = Button(root, text="Save", command=save)
button.grid(row = 0, column = 1, sticky = W)
button.config(bd = 0, bg = "#e5e6e8")


def saveAs():
    global text
    t = text.get("1.0", "end-1c")
    saveLocation = filedialog.asksaveasfilename()
    theFile = open(saveLocation, "w+")
    theFile.write(t)
    theFile.close()
button = Button(root, text="Save as", command=saveAs)
button.grid(row = 0, column = 2, sticky = W)
button.config(bd = 0, bg = "#e5e6e8")

text.grid(row = 1, column = 2)
text.config(bd = 0)

name = Label(root, text="Notepad 1.0")
name.grid(column = 2, row = 0)
name.config(font = "Verdana")

def changeFont(newFont):
    global text
    text.config(font=newFont)
changeFont("Consolas 11")

scrollbar = Scrollbar(root)
scrollbar.grid(column = 3, row = 1, sticky = "NS")

scrollbar.config(command=text.yview)

root.mainloop()